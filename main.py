from code_module import *
from random import randint


ts = [randint(0, 100) for i in range(5)]  # Генератор списка случайных чисел, в range пистаь кол-во

print(f'Исходный список - {ts}')

print('-----')
temp = delta(ts)
print(f'Результат delta-кодировки:\n{temp}')
temp = decode_delta(temp)
print(f'Результат delta-декодировки:\n{temp}')

print('-----')

symbols_2freq = collections.Counter(ts)  # для красивого принта, можешь удалить
huff = fano(ts)
print("Symbol\tWeight\tHuffman Code")
for p in huff:
    print("%s\t%s\t%s" % (p[0], symbols_2freq[p[0]], p[1]))
print('-----')
