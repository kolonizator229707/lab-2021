import random
import string
import module_coders
import time


file = open('timers_coder.txt', 'w')
print('Name\tRange\tBest_time\tLoser_time\tMedium_time', file=file)


def mass_to_file(name, rng, timings):
    best_time = str(min(timings)).replace('.', ',')
    loser_time = str(max(timings)).replace('.', ',')
    medium_time = str(round(sum(timings)/len(timings), 5)).replace('.', ',')
    print('{}\t{}\t{}\t{}\t{}'.format(name, rng, best_time, loser_time, medium_time), file=file)


def generate_random_string(length):
    letters = string.ascii_lowercase
    rand_string = ''.join(random.choice(letters) for i in range(length))
    return rand_string


def huff_time(symbols):
    start = time.time()
    module_coders.huffman_encode(symbols)
    end = time.time()
    return round(end - start, 5)


def rle_time(symbols):
    start = time.time()
    module_coders.rle(symbols)
    end = time.time()
    return round(end - start, 5)


def tester(rng):
    huff_t, rle_t = [], []
    for i in range(5):
        data = generate_random_string(rng)
        huff_t.append(huff_time(data))
        rle_t.append(rle_time(data))
    mass_to_file('huffman', rng, huff_t)
    mass_to_file('rle', rng, rle_t)
    print(file=file)


for i in range(1000, 11000, 1000):
    print(i)
    tester(i)

file.close()